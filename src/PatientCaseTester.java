import java.text.SimpleDateFormat;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Class Description</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class PatientCaseTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		long aCaseID;
		String anAdmReason;
		String anAdmDate;
		int aSize;
		int aWeight; 
		String DateOfBirth;
		char aGender; 
		
		String[][] testCases= {
				{"1", "Unfall 1", "16.08.2011", "170", "65", "20.07.1982", "f"},
				{"1", "Unfall 1", "16.08.2011", "170", "65", "20.07.1982", "f"},
		};
		
		for (int i = 0; i < testCases.length; i++) {

			aCaseID = Long.parseLong(testCases[i][0]);
			anAdmReason = testCases[i][1]; 
			anAdmDate = testCases[i][2]; 
			aSize = Integer.parseInt(testCases[i][3]);
			aWeight = Integer.parseInt(testCases[i][4]);
			DateOfBirth = testCases[i][5]; 
			aGender = testCases[i][6].charAt(0);
			
			PatientCase aPateintCase1 = new PatientCase(aCaseID, anAdmReason, 
					anAdmDate, aSize, aWeight, DateOfBirth, aGender);
			
			System.out.println("getAdmissionDate(): " + aPateintCase1.getAdmissionDate());
			System.out.println("getAdmissionReason(): " + aPateintCase1.getAdmissionReason());
			System.out.println("getAgeInYears(): " + aPateintCase1.getAgeInYears());
			System.out.println("getBMI(): " + aPateintCase1.getBMI());
			System.out.println("getBSA(): " + aPateintCase1.getBSA());
			System.out.println("getCaseData(): " + aPateintCase1.getCaseData());
			System.out.println("getCaseID(): " + aPateintCase1.getCaseID());
			System.out.println("getDueDate(): " + aPateintCase1.getDueDate());
			System.out.println("getDueDateNaegele(): " + aPateintCase1.getDueDateNaegele());
			System.out.println("getSize(): " + aPateintCase1.getSize());
			System.out.println("getWeight(): " + aPateintCase1.getWeight());
		}
		
	}

}
